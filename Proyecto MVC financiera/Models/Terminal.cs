﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class Terminal
    {
        ServiceReference1.ServicioPublicoCreditoClient SW;

        public Terminal()
        {
            SW = new ServiceReference1.ServicioPublicoCreditoClient();
        }


        public ServiceReference1.ResultadoEstadoCliente OtenerEstadoCliente(string codigoFinanciera, int dni)
        {
            return SW.ObtenerEstadoCliente(codigoFinanciera, dni);
        }
        public ServiceReference1.ResultadoOperacion InformarCreditoOtorgado(string codigoFinanciera, int dni, string num,double monto)
        {
            return SW.InformarCreditoOtorgado(codigoFinanciera, dni, num,monto);
        }
        public ServiceReference1.ResultadoOperacion InformarCreditoFinalizado(string codigoFinanciera, int dni, string num)
        {
            return SW.InformarCreditoFinalizado(codigoFinanciera, dni, num);
        }
    }
}