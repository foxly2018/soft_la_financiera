﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class Empleado
    {
        public Empleado()
        {

        }
        public Empleado(int v1, string v2, string v3, Terminal terminal)
        {
            this.Legajo = v1;
            this.Nombre = v2;
            this.Apellido = v3;
            Terminal = terminal;
        }
        public Empleado(int v1, string v2, string v3)
        {
            this.Legajo = v1;
            this.Nombre = v2;
            this.Apellido = v3;
        }

        public int Legajo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public Terminal Terminal { get; set; }

        public Pago CrearPago(Cliente cliente ,double monto)
        {
            var pago = new Pago(cliente, this, monto);
            return pago;
        }
    }
}