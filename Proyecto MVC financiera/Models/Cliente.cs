﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class Cliente
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Telefono { get; set; }
        public int Dni { get; set; }
        public string Domicilio { get; set; }
        public double Sueldo { get; set; }

        public Cliente()
        {

        }

        public Cliente(string nombre, string apellido, int tel, int dni, string domicilio, double sueldo)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Telefono = tel;
            this.Dni = dni;
            this.Domicilio = domicilio;
            this.Sueldo = sueldo;
        }
    }
}