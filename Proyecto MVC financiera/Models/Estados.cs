﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public enum Estados
    {
        pendiente,
        activo,
        finalizado,
        moroso,
        pendienteDeFinalización
    }
}