﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public sealed class Repositorio
    {
        
            private readonly static Repositorio _instance = new Repositorio();

            private Repositorio()
            {
            
            }

            public static Repositorio Instance
            {
                get
                {
                    return _instance;
                }
            }

        public Financiera Financiera { get; set; }
        public List<Empleado> Empleados = new List<Empleado>();
        public List<Cliente> Clientes = new List<Cliente>();
        public List<Credito> Creditos = new List<Credito>();
        public List<Plan> Planes { get; set; }
        public CuotaAdelantada Plan1 { get; set; }
        public CuotaAdelantada Plan2 { get; set; }
        public CuotaVencida Plan3 { get; set; }
        public CuotaVencida Plan4 { get; set; }
        public Credito C1 { get; set; }
        public Credito C2 { get; set; }
        public Credito C3 { get; set; }
        public List<Pago> Pagos { get; set; }

        public void Init()
        {
            Pagos = new List<Pago>();

            Financiera = new Financiera();
            Random rnd = new Random();
            //var terminal = new Terminal();
            //CREACION DE PLANES
            Plan1 = new CuotaAdelantada("Este plan es de metodologia primera cuota adelantada contara con 3 cuotas", 1, Financiera.GastosAdm, 3);
            Plan2 = new CuotaAdelantada("Este plan es de metodologia primera cuota adelantada contara con 5 cuotas", 2, Financiera.GastosAdm, 5);
            Plan3 = new CuotaVencida("Este plan es de metodologia Cuota vncida contara con 6 cuotas", 3, Financiera.GastosAdm, 6);
            Plan4 = new CuotaVencida("Este plan es de metodologia Cuota vncida contara con 4 cuotas", 4, Financiera.GastosAdm, 4);
            Planes = new List<Plan>
            {
                Plan1,
                Plan2,
                Plan3,
                Plan4
            };
            //CRECION DE EMPLEADOS
            Empleados.Add(new Empleado(1234, "Roque", "apellido1"/*, terminal*/));
            Empleados.Add(new Empleado(5678, "Dilacio", "apellido2"/*, terminal*/));
            Empleados.Add(new Empleado(9012, "Esteban", "apellido3"/*, terminal*/));
            //CRECION DE CLIENTES & SUS CREDITOS
            Clientes.Add(new Cliente("Diego", "Medina c1", 123443123, 33000123, "concepcion", 100000.00));
            Clientes.Add(new Cliente("Fabrizio", "Diaz c2", 231241312, 33000456, "Monteros", 200000.00));
            Clientes.Add(new Cliente("Marcelo", "Iturriza c3", 123123127, 33000789, "medina", 300000.00));

            C1 = new Credito(new DateTime(2019, 03, 01),Financiera.Recargo, rnd.Next(5000), 20000.00, 1234, 33000123, Estados.finalizado);
            C2 = new Credito(new DateTime(2018, 08, 06), Financiera.Recargo, rnd.Next(5000), 100000.00, 5678, 33000456, Estados.activo);
            C3 = new Credito(new DateTime(2018, 03, 09), Financiera.Recargo, rnd.Next(5000), 50000.00, 9012, 33000789, Estados.activo);

            C1.Plan = Plan1;
            C2.Plan = Plan2;
            C3.Plan = Plan3;

            Creditos.Add(C1);
            Creditos.Add(C2);
            Creditos.Add(C3);
        }
        public void IniciarCreditoRepositorio()
        {
            foreach (var C in Creditos)
            {
                C.CalcularMontoTotal();
                C.AsignarCuotas();
            }
        }
        public bool Autenticar(int legajo)
        {
            var e = false;
            foreach (var o in Empleados)
            {
                if (o.Legajo == legajo)
                {
                    e = true;
                }
            }
            return e;
        }
        //public Empleado ObtenerEmpleado(int legajo)
        //{
        //    var empledo = new Empleado();
        //    foreach (var e in Empleados)
        //    {
        //        if (e.Legajo == legajo)
        //        {
        //            empledo = e;
        //        }
        //    }
        //    return empledo;
        //}
        public void CrearCliente(string nombre, string apellido, int tel, int dni, string domicilio, double sueldo)
        {
            var c = new Cliente
            {
                Nombre = nombre,
                Apellido = apellido,
                Telefono = tel,
                Dni = dni,
                Domicilio = domicilio,
                Sueldo = sueldo
            };
            Clientes.Add(c);
        }
    }

}