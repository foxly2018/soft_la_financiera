﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class Financiera
    {
        public string NombreComercial { get; set; }
        public string RazonSocial { get; set; }
        public string Cuil { get; set; }
        public string Domicilio { get; set; }
        public double Recargo { get; set; }
        public double GastosAdm { get; set; }
        public string CodigoFinanciera { get; set; }
        public double MontoMaximoSolicitado { get; set; }

        //public List<Empleado> Emplados = new List<Empleado>();
        //public List<Cliente> Clientes = new List<Cliente>();
        //public List<Credito> Creditos = new List<Credito>();
        //public List<Plan> Planes { get; set; }
        //public CuotaAdelantada Plan1 { get; set; }
        //public CuotaAdelantada Plan2 { get; set; }
        //public CuotaVencida Plan3 { get; set; }
        //public CuotaVencida Plan4 { get; set; }
        //public Credito C1 { get; set; }
        //public Credito C2 { get; set; }
        //public Credito C3 { get; set; }

        public Financiera()
        {
            NombreComercial = "Financie Aqui!";
            RazonSocial = "Creditos S.R.L";
            Cuil = "34-1231232432-1";
            Domicilio = "Congreso 600";
            Recargo = 0.5;
            GastosAdm = 2;
            CodigoFinanciera = "d6aef0cb-9170-423c-b02a-ad8adba1086a";
            MontoMaximoSolicitado = 100000;

        }
    }
}