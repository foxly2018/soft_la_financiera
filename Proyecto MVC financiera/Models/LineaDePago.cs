﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class LineaDePago
    {
        public Cuota Cuota { get; set; }
        public double MontoCuota { get; set; }
        public int NumeroCredito { get; set; }

        public LineaDePago(Cuota cuota,int num_credito)
        {
            Cuota = cuota;
            NumeroCredito = num_credito;
            MontoCuota = cuota.Saldo;
        }
    }
}