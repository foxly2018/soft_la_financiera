﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class CuotaVencida:Plan
    {
        public CuotaVencida()
        {
        }

        public CuotaVencida(string descripcion, int num, double porcentaje, int cantCuotas) : base(descripcion, num, porcentaje, cantCuotas)
        {
        }
    }
}