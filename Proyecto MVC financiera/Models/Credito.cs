﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class Credito
    {
        public DateTime FechaSolicitud { get; set; }
        public double Interes { get; set; }
        public double MontoTotal { get; set; }
        public int Numero { get; set; }
        public double MontoSolicitado { get; set; }
        public int EmpleadoCreador { get; set; }
        public int ClientePertenece { get; set; }
        public Estados Estado { get; set; }
        public List<Cuota> Cuotas { get; set; }
        public Comprobante Comprobante { get; set; }
        public Plan Plan { get; set; }

        //Contructores
        public Credito(DateTime dateTime, double interes, int num, double nomtoSolicitado, int legajo, int dni, Estados estado/*,Plan plan*/)
        {
            this.FechaSolicitud = dateTime;
            this.Interes = interes;
            this.Numero = num;
            this.MontoSolicitado = nomtoSolicitado;
            EmpleadoCreador = legajo;
            ClientePertenece = dni;
            Estado = estado;
            //Plan = Plan;
        }
        public Credito()
        {

        }

        //Motodos Credito
        public void CrearCredito(double monto, int plan, int dni, int leg)
        {
            var fecha = DateTime.Now;
            var numCredito = new Random();

            var datosPlan = Repositorio.Instance.Planes.Where(x => x.NumPlan == plan).FirstOrDefault();

            ClientePertenece = dni;
            FechaSolicitud = fecha;
            MontoSolicitado = monto;
            Interes = Repositorio.Instance.Financiera.Recargo;
            Plan = datosPlan;
            EmpleadoCreador = leg;
            Numero = numCredito.Next(5000);

            CalcularMontoTotal(Plan,Interes,MontoSolicitado);

            AsignarCuotas();
        }

        public double CalcularMontoTotal(Plan Plan, double Interes, double MontoSolicitado)
        {
            var gastoAdm = Plan.Porcentaje;
            var interes = Interes;
            var montoSolicitado = MontoSolicitado;

            var valorDescuento = (interes * montoSolicitado) / 100;

            valorDescuento += (gastoAdm * montoSolicitado) / 100;

            return MontoTotal = montoSolicitado - valorDescuento;
        }
        public double CalcularMontoTotal()
        {
            var gastoAdm = Plan.Porcentaje;
            var interes = Interes;
            var montoSolicitado = MontoSolicitado;

            var valorDescuento = (interes * montoSolicitado) / 100;

            valorDescuento += (gastoAdm * montoSolicitado) / 100;

            return MontoTotal = montoSolicitado - valorDescuento;
        }
        public void AsignarCuotas()
        {
            var num = new Random();

            Cuotas = new List<Cuota>();
            int año = 0;
            for (int i = 1; i <= Plan.CantCuotas; i++)
            {                
                var mes = DateTime.Today.Month + i;
                //año = DateTime.Now.Year;
                if (año > DateTime.Now.Year)
                {
                    año = año;
                }
                else
                {
                    año = DateTime.Now.Year;
                }
                if (mes > 12)
                {
                    mes = mes - 12;
                    if (mes == 1) { año = DateTime.Now.Year + 1; };
                }
                if (this.Estado.Equals(Estados.finalizado))
                {
                    Cuotas.Add(new Cuota
                    {
                        Monto = MontoTotal / Plan.CantCuotas,
                        NumCuota = num.Next(),
                        Vencimiento = new DateTime(año, mes, 10),
                        Saldo = 0,
                    });
                }
                else
                {
                    Cuotas.Add(new Cuota
                    {
                        Monto = MontoTotal / Plan.CantCuotas,
                        NumCuota = num.Next(),
                        Vencimiento = new DateTime(año, mes, 10),
                        Saldo = MontoTotal / Plan.CantCuotas,
                    });
                }
                
            }
        }
    }
}