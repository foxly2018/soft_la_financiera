﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class Pago
    {
        public double Monto { get; set; }
        public DateTime Fecha { get; set; }
        public Cliente Cliente { get; set; }
        public Empleado Empleado { get; set; }
        public double Total { get; set; }

        public List<LineaDePago> LineaDePagos { get; set; }

        public Pago(Cliente cliente, Empleado empleado,double monto)
        {
            Monto = monto;
            Cliente = cliente;
            Empleado = empleado;
            Fecha = DateTime.Now;
            LineaDePagos = new List<LineaDePago>();
        }
        public void CrearLinea(Cuota cuota,int num_credido)
        {
            var lp = new LineaDePago(cuota, num_credido);
            LineaDePagos.Add(lp);
        }
        public void CalcularTotal()
        {
            double tot = 0;
            foreach (var lp in LineaDePagos)
            {            
                tot += lp.Cuota.Saldo;
            }
            Total = tot;
        }
        public void Pagar()
        {
            double auxMonto = Monto;
            foreach (var lp in LineaDePagos)
            {               
                if (lp.Cuota.Saldo <= auxMonto)
                {
                    auxMonto = auxMonto - lp.Cuota.Saldo;
                    lp.Cuota.Saldo = 0;                    
                }
                else
                {
                    lp.Cuota.Saldo = lp.Cuota.Saldo - auxMonto;
                }
            }
        }

        public void VerificarEstadosCreditos(Cliente cliente)
        {
            foreach (var lp in LineaDePagos)
            {
                var cretido = Repositorio.Instance.Creditos.Where(c => c.Numero == lp.NumeroCredito).FirstOrDefault();

                var cuotas_pagadas = cretido.Cuotas.Where(cu => cu.Saldo == 0).ToList();

                if (cuotas_pagadas.Count == cretido.Cuotas.Count)
                {
                    var validacion = Empleado.Terminal.InformarCreditoFinalizado(Repositorio.Instance.Financiera.CodigoFinanciera, cliente.Dni, cretido.Numero.ToString());
                    
                    if (validacion.OperacionValida)
                    {
                        cretido.Estado = Estados.finalizado;
                    }
                    else
                    {
                        cretido.Estado = Estados.pendienteDeFinalización;
                    }
                }
                else
                {
                    // verificar si deja de estar moroso en el caso q lo este
                }

            }
        }
    }

}