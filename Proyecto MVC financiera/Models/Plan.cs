﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_MVC_financiera.Models
{
    public class Plan
    {
        public string Descripcion { get; set; }
        public int NumPlan { get; set; }
        public double Porcentaje { get; set; }
        public int CantCuotas { get; set; }

        public Plan(string descripcion, int num, double porcentaje, int cantCuotas)
        {
            Descripcion = descripcion;
            NumPlan = num;
            Porcentaje = porcentaje;
            CantCuotas = cantCuotas;
        }

        public Plan()
        {
        }
    }
}