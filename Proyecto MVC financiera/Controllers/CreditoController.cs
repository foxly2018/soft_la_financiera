﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_MVC_financiera.Models;

namespace Proyecto_MVC_financiera.Controllers
{
    public class CreditoController : Controller
    {
         
        /*SECCION SOLICITUD CREDITO*/
        public ActionResult Credito()
        {
            if (TempData.ContainsKey("legajo"))
            {
                var leg = (int)TempData["legajo"];
                TempData["legajo"] = leg;
            }
            ViewBag.Planes = new SelectList(Repositorio.Instance.Planes, "NumPlan", "Descripcion");
            return View();
        }
        [HttpGet]
        public ActionResult EstadoCliente(int dni)
        {
            try
            {
                int leg = 0;
                if (TempData.ContainsKey("legajo"))
                {
                    leg = (int)TempData["legajo"];
                    TempData["legajo"] = leg;
                }
                var c = Repositorio.Instance.Clientes.Where(x => x.Dni == dni).FirstOrDefault();
                var empleado = Repositorio.Instance.Empleados.Where(e => e.Legajo == leg).FirstOrDefault();

                //var estado = empleado.Terminal.OtenerEstadoCliente(Repositorio.Instance.Financiera.CodigoFinanciera, c.Dni);

                if (false /*estado.CantidadCreditosActivos > 3 || estado.TieneDeudas == true*/)
                {
                    return Json(new
                    {
                        mjs = "Este Cliente no puede obtener un nuevo credito!!",
                        alert = "alert alert-danger",
                        validacion = false,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        mjs = "Puede otorgar un nuevo credito al Cliente!!",
                        alert = "alert alert-success",
                        validacion = true,
                    }, JsonRequestBehavior.AllowGet);
                }
                //return RedirectToAction("Credito");
            }
            catch (Exception ex)
            {
                throw ex;
                //return View();
            }
        }

        [HttpPost]
        public ActionResult CrearCredito(double monto, int plan, int dni)
        {
            try
            {
                int leg = 0;
                if (TempData.ContainsKey("legajo"))
                {
                    leg = (int)TempData["legajo"];
                    TempData["legajo"] = leg;
                }
                string Mjs;
                string Alert;
                bool Validacion = false;
                bool Abonar = false;
                int numCredi = 0;
                if (monto <= Repositorio.Instance.Financiera.MontoMaximoSolicitado)
                {
                    var credito = new Credito();
                    credito.CrearCredito(monto, plan, dni, leg);

                    var empleado = Repositorio.Instance.Empleados.Where(e => e.Legajo == leg).FirstOrDefault();
                    //var informeCreditoOtorgado = empleado.Terminal.InformarCreditoOtorgado(Repositorio.Instance.Financiera.CodigoFinanciera, dni, credito.Numero.ToString(), credito.MontoTotal);

                    //credito.Estado = informeCreditoOtorgado.OperacionValida == false ? Estados.pendiente : Estados.activo;

                    if (true/*credito.Estado.Equals(Estados.activo)*/)
                    {
                        Mjs = "El Credito esta " + "Activo";//credito.Estado.ToString();
                        Alert = "alert alert-success";
                        Validacion = true;
                    }
                    else
                    {
                        Mjs = "El Credito esta " + "inactivo";//credito.Estado.ToString();
                        Alert = "alert alert-warning";
                        Validacion = false;
                    }
                    if (credito.Plan.GetType() == typeof(CuotaAdelantada))
                    {
                        Abonar = true;
                    }
                    Repositorio.Instance.Creditos.Add(credito);
                    numCredi = credito.Numero;
                    return Json(new
                    {
                        mjs = Mjs,
                        alert = Alert,
                        abonar = Abonar,
                        validacion = Validacion,
                        botonImprimir = numCredi,
                    }, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("Credito");
                }
                else
                {
                    return Json(new
                    {
                        mjs = "El monto no puede ser super a los $100.000!!",
                        alert = "alert alert-danger",
                        abonar = Abonar,
                        validacion = false,
                        botonImprimir = numCredi,
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //return View();
            }
        }

        [HttpGet]
        public ActionResult ImprimirComprobante(int numCredito)
        {
            var credito = Repositorio.Instance.Creditos.Where(c => c.Numero == numCredito).FirstOrDefault();

            return PartialView(credito);
        }
    }
}
