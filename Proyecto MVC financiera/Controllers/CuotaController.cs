﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_MVC_financiera.Models;

namespace Proyecto_MVC_financiera.Controllers
{
    public class CuotaController : Controller
    {
        public ActionResult Cuota()
        {
            if (TempData.ContainsKey("legajo"))
            {
                var leg = (int)TempData["legajo"];
                TempData["legajo"] = leg;
            }
            return View();
        }

        public ActionResult GetDatosCreditos(int dni,int formaDePago)
        {
            if (TempData.ContainsKey("legajo"))
            {
                var leg = (int)TempData["legajo"];
                TempData["legajo"] = leg;
            }
            var lstCred = Repositorio.Instance.Creditos.Where(c => c.ClientePertenece == dni).ToList();
            int cont = 0;

            foreach (var credito in lstCred)
            {
                foreach (var cuota in credito.Cuotas)
                {
                    if (cuota.Saldo != 0 && cuota.Vencimiento < DateTime.Now)
                    {
                        cont += 1;
                        TimeSpan ts = DateTime.Now - cuota.Vencimiento;
                        int diasVencidos = ts.Days;
                        cuota.Saldo = cuota.Monto + (cuota.Monto * (credito.Interes * diasVencidos)) / 100;
                        if (cont >= 2)
                        {
                            credito.Estado = Estados.moroso;
                        }
                    }
                }
                cont = 0;
            }
            switch (formaDePago)
            {
                case 0:
                    return PartialView("GetDatosCreditos",lstCred);

                case 1:
                    return PartialView("GetDatosCreditos",lstCred);
                
                case 2:
                    return PartialView("GetPagoCreditoAll",lstCred);
                default:
                    break;
            }
            return PartialView(lstCred);
        }
        [HttpPost]
        public ActionResult Pago(List<JsonPagos> JsonPagos, double monto,int  dni)
        {
            int legajo =0;
            if (TempData.ContainsKey("legajo"))
            {
                legajo = (int)TempData["legajo"];
                TempData["legajo"] = legajo;
            }
            try
            {
                var empleado = Repositorio.Instance.Empleados.Where(x => x.Legajo== legajo).FirstOrDefault();
                var cliente = Repositorio.Instance.Clientes.Where(c => c.Dni == dni).FirstOrDefault();

                var p = empleado.CrearPago(cliente, monto);

                foreach (var pagar in JsonPagos)
                {
                    var Num_Credito = pagar.NumeroCredito;
                    var Num_Cuota = pagar.NumeroCuota;
                    var cuota = Repositorio.Instance.Creditos.Where(c => c.Numero == Num_Credito).FirstOrDefault().Cuotas.Where(cu => cu.NumCuota == Num_Cuota).FirstOrDefault();
                    p.CrearLinea(cuota, Num_Credito);
                }

                p.CalcularTotal();

                p.Pagar();
                p.VerificarEstadosCreditos(cliente);

                Repositorio.Instance.Pagos.Add(p);
                return PartialView("ReciboPago");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
