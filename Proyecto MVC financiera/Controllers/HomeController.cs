﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Proyecto_MVC_financiera.Models;

namespace Proyecto_MVC_financiera.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Repositorio.Instance.IniciarCreditoRepositorio();
            ViewBag.error = "none";
            return View();
        }
        //utilizo este metodo para la principal de logeado del empleado
        public ActionResult About(FormCollection login)
        {
            if (login["pwd"] != null)
            {
                int legajo = int.Parse(login["pwd"]);

                if (Repositorio.Instance.Autenticar(legajo))
                {
                    TempData["legajo"] = legajo;
                    ViewBag.error = "none";
                    ViewBag.Empleado = Repositorio.Instance.Empleados.Where(e => e.Legajo == legajo).FirstOrDefault();
                    ViewBag.Message = "Logeo exitoso.";
                    return View();
                }
                else
                {
                    ViewBag.error = "block";
                    return View("Index");
                }
            }
            else
            {
                ViewBag.error = "block";
                return View("Index");
            }            

        }
        //utilizo este metodo para mandar y listar los clientes en la vista
        public ActionResult Contact()
        {
            var c =  Repositorio.Instance.Clientes;
            return View(c);
        }

        /**SECCION PARA LA GESTION CLIENTES***/

        // GET: Clientes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clientes/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var cliente = new Cliente(collection["Nombre"], collection["Apellido"], int.Parse(collection["Telefono"]), int.Parse(collection["Dni"]), collection["Domicilio"], double.Parse(collection["Sueldo"]));
                Repositorio.Instance.Clientes.Add(cliente);
                return RedirectToAction("Contact");
            }
            catch
            {
                return View();
            }
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Clientes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Clientes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}