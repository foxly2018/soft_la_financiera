﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Proyecto_MVC_financiera.Controllers;
using Proyecto_MVC_financiera.Models;

namespace LaFinanciera.test
{
    [TestClass]
    public class UnitTest1
    {
        //[TestMethod]
        //public void TestCreditoActivo()
        //{
        //    //preparacion
        //    double monto = 20000;
        //    int plan = 3;
        //    int dni = 33000123;
        //    Repositorio.Instance.Init();
        //    CreditoController CreC = new CreditoController();

        //    //ejecucion
        //    JsonResult resultado = CreC.CrearCredito(monto, plan, dni) as JsonResult;
        //    IDictionary<string, object> diccionario = (IDictionary<string, object>) new RouteValueDictionary(resultado.Data);

        //    //validacion
        //    Assert.AreEqual(true, diccionario["Validacion"], "");
        //}
        [TestMethod]
        public void TestCreacionDeEmpleado()
        {
            //preparacion
            int legajo = 2234;
            string nombre = "Jorge";
            string apellido = "Enrrique";

            //ejecucion
            var e = new Empleado(legajo,nombre,apellido);

            //validacion
            Assert.IsNotNull(e);
        }
        [TestMethod]
        public void TestCantidadDeCuotasInstanciadasEnCredito()
        {
            //preparacion
            Repositorio.Instance.Init();
            double monto = 20000;
            int plan = 3;
            int dni = 33000123;
            int leg = 1234;
            var credito = new Credito();

            //ejecucion
            credito.CrearCredito(monto, plan, dni, leg);

            //validacion
            Assert.AreEqual(6, credito.Cuotas.Count);
        }
        //[TestMethod]
        //public void TestEstadoCliente()
        //{
        //    //preparacion
        //    int dni = 33000123;
        //    Repositorio.Instance.Init();
        //    CreditoController CreC = new CreditoController();

        //    //ejecucion
        //    JsonResult resultado = CreC.EstadoCliente(dni) as JsonResult;
        //    IDictionary<string, object> diccionario = (IDictionary<string, object>)new RouteValueDictionary(resultado.Data);

        //    //validacion
        //    Assert.AreEqual("Puede otorgar un nuevo credito al Cliente!!", diccionario["mjs"], "");
        //}
        [TestMethod]
        public void TestCalcularMontoTotalDelCredito()
        {
            //preparacion
            Repositorio.Instance.Init();
            double MontoSolicitado = 50000;
            var Plan = Repositorio.Instance.Planes.Where(x => x.NumPlan == 3).FirstOrDefault();
            var Interes = Repositorio.Instance.Financiera.Recargo;
            var c = new Credito();

            //ejecucion
            var total = c.CalcularMontoTotal(Plan, Interes, MontoSolicitado);

            //validacion
            Assert.AreEqual(48750, total,"");
        }
        [TestMethod]
        public void TestAutenticarLegajoEmpleado()
        {
            //preparacion
            Repositorio.Instance.Init();
            int legajo = 1234;

            //ejecucion
            bool resultado = Repositorio.Instance.Autenticar(legajo);

            //validacion
            Assert.AreEqual(true, resultado);
        }
        [TestMethod]
        public void TestCrearNuevoPago()
        {
            //preparacion
            Repositorio.Instance.Init();
            Cliente cliente = Repositorio.Instance.Clientes.Where(c => c.Dni == 33000456).FirstOrDefault();
            Empleado empleado = Repositorio.Instance.Empleados.Where(e => e.Legajo == 5678).FirstOrDefault();
            double monto = 40000;

            //ejecucion
            Pago p = empleado.CrearPago(cliente, monto);

            //validacion
            Assert.IsNotNull(p);
        }
    }
}
